namespace kernel;

class AccountBalanceTests {
    [Test]
    public void Compute_balance_of_several_operations() {
        var today = new LocalDate();
        var operations = new List<Operation>() {
            Operation.Deposit(today, 4u),
            Operation.Withdraw(today, 1u)
        };

        var balance = AccountBalance.Compute(operations);

        Assert.That(balance, Is.EqualTo(3));
    }
}