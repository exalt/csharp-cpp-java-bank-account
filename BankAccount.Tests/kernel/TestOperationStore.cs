namespace kernel;

class TestOperationStore : IOperationStore
{
    private readonly List<Operation> operations;

    internal TestOperationStore() {
        operations = new List<Operation>();
    }

    public void Append(Operation operation)
    {
        operations.Add(operation);
    }

    public IEnumerable<Operation> RetrieveAll()
    {
        return operations;
    }
}