namespace kernel;

class CheckingAccountTests
{

    [TestCase(0u)]
    [TestCase(1u)]
    [TestCase(2u)]
    [TestCase(10u)]
    public void Making_a_deposit_increases_balance(uint amount)
    {
        var sut = CreateSut();

        sut.Deposit(amount);

        Assert.That(sut.Balance, Is.EqualTo(amount));
    }

    [Test]
    public void Making_several_deposits_increases_the_balance()
    {
        var sut = CreateSut();

        sut.Deposit(1);
        sut.Deposit(0);
        sut.Deposit(4);
        sut.Deposit(2);

        Assert.That(sut.Balance, Is.EqualTo(7L));
    }

    [TestCase(0u)]
    [TestCase(2u)]
    [TestCase(5u)]
    [TestCase(7u)]
    public void Making_a_withdrawal_decreases_balance(uint amount)
    {
        var sut = CreateSut();

        sut.Withdraw(amount);

        Assert.That(sut.Balance, Is.EqualTo(-1L * amount));
    }

    [Test]
    public void Making_several_operations_impacts_the_balance()
    {
        var sut = CreateSut();
        sut.Deposit(1);
        sut.Withdraw(2);
        sut.Deposit(4);
        sut.Withdraw(5);

        Assert.That(sut.Balance, Is.EqualTo(-2L));
    }

    [Test]
    public void Statement_contains_all_operations()
    {
       var sut = CreateSut();
       var depositAmount = 11u;
       var withdrawalAmount = 9u;
       
       sut.Deposit(depositAmount);
       sut.Withdraw(withdrawalAmount);

       var today = new LocalDate();
       var deposit = Operation.Deposit(today, depositAmount);
       var withdrawal = Operation.Withdraw(today, withdrawalAmount);

       var statement = sut.Statement();

       Assert.That(statement, Contains.Item(deposit));
       Assert.That(statement, Contains.Item(withdrawal));
       Assert.That(statement.Balance, Is.EqualTo(2L));
    }

    private CheckingAccount CreateSut()
    {
        var clock = new TestClock { Today = new LocalDate() };
        var operationStore = new TestOperationStore();

        return new CheckingAccount(clock, operationStore);
    }
}
