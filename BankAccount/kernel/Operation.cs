namespace kernel;

public record class Operation(LocalDate Date, OperationKind Kind, uint Amount)
{

    public static Operation Deposit(LocalDate date, uint amount) {
        return new Operation(date, OperationKind.Deposit, amount);
    }
        
    public static Operation Withdraw(LocalDate date, uint amount) {
        return new Operation(date, OperationKind.Withdrawal, amount);
    }
}

public enum OperationKind
{
    Deposit,
    Withdrawal
}