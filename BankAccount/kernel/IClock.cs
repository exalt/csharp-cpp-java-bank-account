namespace kernel;

public interface IClock
{
    LocalDate Today { get; }
}