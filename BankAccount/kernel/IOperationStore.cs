namespace kernel;

public interface IOperationStore
{
    void Append(Operation operation);

    IEnumerable<Operation> RetrieveAll();
}