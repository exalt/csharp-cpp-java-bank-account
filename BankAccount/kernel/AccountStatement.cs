using System.Collections;

namespace kernel;

public class AccountStatement : IEnumerable<Operation>
{
    private readonly LocalDate date;
    private readonly List<Operation> operations;
    private readonly long balance;

    public AccountStatement(LocalDate date, long balance, params Operation[] operations)
    {
        this.date = date;
        this.balance = balance;
        this.operations = new List<Operation>(operations);
    }

    public AccountStatement(LocalDate date, long balance, IEnumerable<Operation> operations)
    {
        this.date = date;
        this.balance = balance;
        this.operations = new List<Operation>(operations);
    }

    public LocalDate Date { get { return date; } }

    public long Balance { get { return balance; } }

    public IEnumerator<Operation> GetEnumerator()
    {
        return ((IEnumerable<Operation>)operations).GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return ((IEnumerable)operations).GetEnumerator();
    }
}