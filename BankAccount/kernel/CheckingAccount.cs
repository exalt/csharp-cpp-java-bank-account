namespace kernel;

public class CheckingAccount : IUnspecifiedPort
{
    private readonly IClock clock;
    private readonly IOperationStore operationStore;

    public CheckingAccount(
        IClock clock,
        IOperationStore operationStore)
    {
        this.clock = clock;
        this.operationStore = operationStore;
    }

    public void Deposit(uint amount)
    {
        var today = clock.Today;
        var operation = Operation.Deposit(today, amount);

        AppendOperation(operation);
    }

    public void Withdraw(uint amount)
    {
        var today = clock.Today;
        var operation = Operation.Withdraw(today, amount);

        AppendOperation(operation);
    }

    private void AppendOperation(Operation operation)
    {
        operationStore.Append(operation);
    }

    public AccountStatement Statement()
    {
        var today = clock.Today;
        var operations = operationStore.RetrieveAll();
        var balance = AccountBalance.Compute(operations);

        return new AccountStatement(today, balance, operations);
    }

    public long Balance
    {
        get 
        {
            var operations = operationStore.RetrieveAll();
            var balance = AccountBalance.Compute(operations);

            return balance;
        } 
    }
}