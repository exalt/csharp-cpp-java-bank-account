namespace kernel;

public static class AccountBalance
{
    public static long Compute(IEnumerable<Operation> operations)
    {
        return operations.Sum(computeImpact);
    }

    private static long computeImpact(Operation operation)
    {
        return operation.Kind == OperationKind.Deposit
            ? operation.Amount * 1L
            : operation.Amount * -1L;
    }
}