namespace kernel;

public interface IUnspecifiedPort
{
    void Deposit(uint amount);

    void Withdraw(uint amount);

    long Balance { get; }
}